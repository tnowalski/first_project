package com.TomaszNowalski.ProjektTechnologieJE;

import com.TomaszNowalski.ProjektTechnologieJE.controlers.SecurityController;
import com.TomaszNowalski.ProjektTechnologieJE.repositorys.FirmaRepository;
import com.TomaszNowalski.ProjektTechnologieJE.repositorys.OsobaRepository;
import com.TomaszNowalski.ProjektTechnologieJE.services.FirmaService;
import com.TomaszNowalski.ProjektTechnologieJE.services.OsobaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;


@Component
@Scope("singleton")
public class Starter implements CommandLineRunner {

    @Autowired
    OsobaRepository osobaRepository;

    @Autowired
    FirmaRepository firmaRepository;

    @Autowired
    FirmaService firmaService;

    @Autowired
    OsobaServices osobaServices;

    @Autowired
    Mailer mailer;

    @Autowired
    SecurityController securityController;

    @Autowired
    EntityManager em;


    @Override
    @Transactional
    public void run(String... args) throws Exception {

/*
        osobaRepository.AddOsoba("Ad","Ad", "130tomek@wp.com", "Password");
       osobaRepository.AddOsoba("Jan", "Kowalski", "1@1.1", "pass");
        osobaRepository.AddOsoba("Jan", "Makowski", "adres@wp.pl", "pass");
        osobaRepository.AddOsoba("Piotr", "Kwiatkowski", "InnyAdres@wp.pl", "pass");
        osobaRepository.AddOsoba("Kacper", "Plikowski", "KN@gmail.com", "pass");
        osobaRepository.AddOsoba("Tomasz", "Majerczyk", "TomMa00@wp.pl", "pass");
        osobaRepository.AddOsoba("Kacper", "Pec", "MojaSkrzynak@onet.pl", "pass");
        firmaRepository.AddFirma("Company", 2);
        firmaRepository.AddFirma("Murbet", 3);
        firmaRepository.AddFirma("CarBUD", 3);
        osobaRepository.AddFirmaToOsoba("Murbet", 1);
        osobaRepository.AddFirmaToOsoba("Murbet", 4);
        osobaRepository.AddFirmaToOsoba("CarBUD", 5);
        List<User> list =  em.createQuery("select u from User u where u.role like 'ADMIN' and u.name = 'Admin@admin.com'",User.class).getResultList();
        if(list.size() == 0){
            osobaRepository.AddOsoba("Admin","Admin", "Admin@admin.com", "Password1!");
            User user = em.createQuery("select u from User u where u.name = 'Admin@admin.com'",User.class).getSingleResult();
            user.setRole("ADMIN");
            user.setEnable(true);
            em.merge(user);
        }

*/


    }

}
