package com.TomaszNowalski.ProjektTechnologieJE.repositorys;

import com.TomaszNowalski.ProjektTechnologieJE.Mailer;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Firma;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.Osoba;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.OsobaDtoPress;
import com.TomaszNowalski.ProjektTechnologieJE.basicClass.User;
import com.TomaszNowalski.ProjektTechnologieJE.controlers.IndexController;
import com.TomaszNowalski.ProjektTechnologieJE.services.FirmaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class OsobaRepository {

    @Autowired
    FirmaRepository firmaRepository;

    @Autowired
    FirmaService firmaService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    IndexController indexController;

    @Autowired
    Mailer mailer;

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void AddOsoba(String name, String lasteName, String email, String password, String userName) {
        Osoba osoba = new Osoba();
        osoba.setImie(name);
        osoba.setNazwisko(lasteName);
        osoba.setEmail(email);
        em.persist(osoba);
        userRepository.AddUser(osoba, password, userName);
    }

    @Transactional
    public void EditOsoba(String name, String lasteName, String email, String password, String userName) {
        boolean flag = false;
        User user = em.createQuery("select u from User u where u.name like '"+ userName +"'", User.class).getSingleResult();
        Osoba osoba = em.find(Osoba.class, user.getOsoba().getId());
        if(!osoba.getImie().equals(name))
            osoba.setImie(name);
        if(!osoba.getNazwisko().equals(lasteName))
            osoba.setNazwisko(lasteName);

        user.setPassword(password);

        if(!osoba.getEmail().equals(email))
        {
            osoba.setEmail(email);
            user.setName(email);
            mailer.sendMail(user.getOsoba().getEmail(), "Potwierdzenie Rejestracji ", "Zarejestrowano użytkownika " + user.getOsoba().getImie() + " " + user.getOsoba().getNazwisko() + "\nLink aktywacyjny: http://localhost:8080/account_active/"+ user.getId());
            flag = true;
        }
        em.merge(osoba);
        em.merge(user);
        if(flag)
            user.setEnable(false);
    }

    @Transactional
    public void RemoveOsoba(int id) {
        for(User user : em.createQuery("select u from User u where u.osoba.id like " + id, User.class).getResultList()) {
            em.remove(user);
        }
        for (Firma firma : em.createQuery("select f from Firma f where f.prezesFirmy.id like " + id, Firma.class).getResultList()) {
            for (Osoba osoba : em.createQuery("select o from Osoba o where o.firma.id like " + firma.getId(), Osoba.class).getResultList()){
                osoba.setFirma(null);
                osoba.setDataZatrudnienia(null);
            }
            em.remove(firma);
        }
        em.remove(em.find(Osoba.class, id));
    }

    @Transactional
    public void RemoveOsobaWithFirma(int id) {
        for (Firma firma : em.createQuery("select f from Firma f where f.prezesFirmy.id like " + id, Firma.class).getResultList()) {
            firma.setPrezesFirmy(null);
            em.merge(firma);
        }
        Osoba osoba = em.find(Osoba.class, id);
        osoba.setFirma(null);
        osoba.setDataZatrudnienia(null);
        em.merge(osoba);
    }


    public Osoba GetOsoba(int id) {
        return em.find(Osoba.class, id);
    }

    public Osoba GetOsoba(String name) {
        return em.createQuery("select u from User u where u.name like '"+ name +"'", User.class).getSingleResult().getOsoba();
    }

    public List<OsobaDtoPress> GetAllOsobaDtoPress() {
        List<Osoba> osobaList = em.createQuery("select o from Osoba o", Osoba.class).getResultList();
        osobaList.remove(GetAdmin());
        List<OsobaDtoPress> osobaDtoPressList = new ArrayList<>();
        for(Osoba osoba : osobaList) {
            List<String> firmaNames = new ArrayList<>();
            for (Firma firma : firmaRepository.GetAllOsobaFirma(osoba.getId()))
                firmaNames.add(firma.getNazwaFirmy());
            osobaDtoPressList.add(new OsobaDtoPress(osoba, IsPress(osoba.getId()), firmaNames));
        }
        return  osobaDtoPressList;
    }

    public List<OsobaDtoPress> GetAllOsobaDtoPress(String name) {
        List<Osoba> osobaList = new ArrayList<>();
        User user = em.createQuery("select u from User u where u.name like '"+ name +"'", User.class).getSingleResult();
        for (Firma firma : em.createQuery("select f from Firma f where f.prezesFirmy.id like " + user.getOsoba().getId(), Firma.class).getResultList()) {
            osobaList.addAll(em.createQuery("select o from Osoba o where o.firma.id like " + firma.getId() , Osoba.class).getResultList());
        }
        osobaList.addAll(em.createQuery("select o from Osoba o where o.firma is null" , Osoba.class).getResultList());
        osobaList.remove(em.find(Osoba.class, user.getOsoba().getId()));
        osobaList.remove(GetAdmin());

        List<OsobaDtoPress> osobaDtoPressList = new ArrayList<>();
        for(Osoba osoba : osobaList)
            osobaDtoPressList.add(new OsobaDtoPress(osoba, false, new ArrayList<>()));
        return  osobaDtoPressList;
    }

    public List<Osoba> GetAllOsoba() {
        List<Osoba> osobaList = em.createQuery("select o from Osoba o", Osoba.class).getResultList();
        osobaList.remove(GetAdmin());
        return  osobaList;
    }

    public List<Osoba> GetAllOsoba(String name) {
        List<Osoba> osobaList = new ArrayList<>();
        User user = em.createQuery("select u from User u where u.name like '"+ name +"'", User.class).getSingleResult();
        for (Firma firma : em.createQuery("select f from Firma f where f.prezesFirmy.id like " + user.getOsoba().getId(), Firma.class).getResultList()) {
            osobaList.addAll(em.createQuery("select o from Osoba o where o.firma.id like " + firma.getId() , Osoba.class).getResultList());
        }
        osobaList.remove(em.find(Osoba.class, user.getOsoba().getId()));
        osobaList.remove(GetAdmin());
        return  osobaList;
    }

    public List<Osoba> GetOsobaOnList(String name) {

            List<Osoba> osobaList = new ArrayList<>();
            User user = em.createQuery("select u from User u where u.name like '"+ name +"'", User.class).getSingleResult();
            Osoba osoba = em.find(Osoba.class, user.getOsoba().getId());
            osobaList.add(osoba);
            return osobaList;
    }

    public List<Osoba> GetOsobaWithoutWork() {
        List<Osoba> osobaList = em.createQuery("select o from Osoba o where o.firma is null ", Osoba.class).getResultList();
        osobaList.remove(GetAdmin());
        return  osobaList;
    }

    @Transactional
    public void AddFirmaToOsoba(String firmaName, int idOsoba) {
        Osoba osoba = em.find(Osoba.class, idOsoba);
        for (Firma firma : em.createQuery("select f from Firma f where f.nazwaFirmy like '" + firmaName + "'", Firma.class).getResultList()) {
            osoba.setFirma(firma);
            osoba.setDataZatrudnienia(new Date());
            em.merge(osoba);
        }
    }

    private Osoba GetAdmin(){
        return em.createQuery("select o from Osoba o where o.email like 'Admin@admin.com'", Osoba.class).getSingleResult();
    }

    public Osoba GetPreses(String name){
        return em.createQuery("select o from Osoba o where o.id like " + firmaRepository.GetFirma(name).getPrezesFirmy().getId(), Osoba.class).getSingleResult();
    }

    public boolean IsPress(int id){
        for(Firma firma : firmaRepository.GetAllFirma())
            if(firma.getPrezesFirmy().getId() == id)
                return true;
        return false;
    }
}
