package com.TomaszNowalski.ProjektTechnologieJE.repositorys;

import com.TomaszNowalski.ProjektTechnologieJE.basicClass.User;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Component
public class SecurityRepository {

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void accountActive(int id){
        User user = em.find(User.class, id);
        user.setEnable(true);
        em.merge(user);
    }

    public String getRoleByName(String name){
        return em.createQuery("select u from User u where u.osoba.email like '" + name + "'", User.class).getSingleResult().getRole();
    }
}
